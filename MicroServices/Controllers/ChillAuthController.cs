﻿using MicroServices.Helpers;
using MicroServices.Models;
using MicroServices.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace MicroServices.Controllers
{
    [Route("users")]
    [ApiController]
    public class ChillAuthController : ControllerBase
    {
        private IUserService _userService;
        private readonly AppSettings _appSettings;
        public ChillAuthController(IUserService userService, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(AuthenticateRequest model)
        {

            try
            {
                //string dcryptpass = _userService.DecryptStringAES(System.Net.WebUtility.UrlDecode(authRequest.password));
                //string hashedData = _userService.ComputeStringToSha256Hash(dcryptpass);
                //var remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                AuthenticateResponse authenticateResponse = _userService.Authenticate(model, HttpContext);
                if (authenticateResponse == null)
                    return BadRequest(new { Message = "Invalid Credentials" });

                var identity = new ClaimsIdentity("Form");
                identity.AddClaim(new Claim(ClaimTypes.Sid, Convert.ToString(authenticateResponse.Token)));
                identity.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(authenticateResponse.SID)));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Employee"));
                var principal = new ClaimsPrincipal(identity);
                Request.HttpContext.Items["MicroUser"] = new AuthTokenResponse { Token = authenticateResponse.Token,
                SID = authenticateResponse.SID, Role = "Employee" };

                await Request.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return Ok(new { authToken = authenticateResponse.Token,
                    ischg = authenticateResponse.ChangePassReq  });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }
             
        }

        [HttpPost("otpauthenticate")]
        public async Task<IActionResult> OTPAuthenticate(AuthenticateOTPRequired model)
        {

            try
            {  

                if (Request.HttpContext.Items["MicroUser"] != null)
                {
                    AuthTokenResponse authToken = (AuthTokenResponse)Request.HttpContext.Items["MicroUser"];

                    AuthenticateOTPResponse authenticateResponse = _userService.OTPAuthenticate(model, authToken.SID);
                    if (authenticateResponse != null)
                    {
                        return Ok(new
                        {
                            authToken = authToken.Token,
                            ischg = authenticateResponse.ChangePassReq
                        });
                    }
                    else
                    {
                        return BadRequest(new { Message = "Invalid OTP" });
                    } 
                }
                else
                {
                    return BadRequest(new { Message = "Invalid User" });
                } 
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });// new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }

        }

        [HttpPost("otpretrigger")]
        public async Task<IActionResult> OTPReTrigger()
        {

            try
            {

                if (Request.HttpContext.Items["MicroUser"] != null)
                {
                    AuthTokenResponse authToken = (AuthTokenResponse)Request.HttpContext.Items["MicroUser"];

                    AuthenticateOTPResponse authenticateResponse = _userService.OTPReTrigger(authToken.SID, HttpContext);
                    if (authenticateResponse != null)
                    {
                        return Ok(new
                        { 
                            ischg = authenticateResponse.ChangePassReq
                        });
                    }
                    else
                    {
                        return BadRequest(new { Message = "Invalid OTP" });
                    }
                }
                else
                {
                    return BadRequest(new { Message = "Invalid User" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });// new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }

        }




        [HttpPost("ssoauthenticate")]
        public async Task<IActionResult> SSOAuthenticate(SSOAuthenticateRequest model)
        {

            try
            {
                string dcryptpass = StringEncryptChiper.Decrypt(HttpUtility.UrlDecode(model.Email), _appSettings.EncryptPassword);
                //string hashedData = _userService.ComputeStringToSha256Hash(dcryptpass);
                //var remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();

                AuthenticateResponse authenticateResponse = _userService.SSOAuthenticate(dcryptpass);
                if (authenticateResponse == null)
                    return BadRequest(new { Message = "Invalid Credentials" });

                var identity = new ClaimsIdentity("Form");
                identity.AddClaim(new Claim(ClaimTypes.Sid, Convert.ToString(authenticateResponse.Token)));
                identity.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(authenticateResponse.SID)));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Employee"));
                var principal = new ClaimsPrincipal(identity);
                Request.HttpContext.Items["MicroUser"] = new AuthTokenResponse
                {
                    Token = authenticateResponse.Token,
                    SID = authenticateResponse.SID,
                    Role = "Employee"
                };

                await Request.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return Ok(new
                {
                    authToken = authenticateResponse.Token,
                    ischg = authenticateResponse.ChangePassReq
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }

        }


        [HttpPost("forgot-password")]
        public IActionResult Forgetpassword(AuthenticateForgetRequest model)
        {
            try
            {
                var response = _userService.ForgetPassword(model, HttpContext);

                if (response == null)
                    return BadRequest(new { message = "Invalid Username" });

                return Ok(response);
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }

        }

        [HttpPost("changepassword")]
        public IActionResult Changepassword(AuthenticateChangePWDRequest model)
        {
            try
            {

                var response = _userService.ChangePassword(model);

                if (response == null)
                    return BadRequest(new { message = "Invalid Access" });

                return Ok(response);
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }
        }

        [HttpPost("logout")]
        public IActionResult Logout()
        {
            try
            {
                 
                if (Request.HttpContext.Items["MicroUser"] != null)
                {
                    AuthTokenResponse authToken = (AuthTokenResponse)Request.HttpContext.Items["MicroUser"];

                    var result = Task.FromResult(_userService.LogOut(authToken.SID));

                    return Ok(result);
                }
                return new JsonResult(new { IsLogOutCheck = true }) { StatusCode = StatusCodes.Status200OK }; 
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }
        }

        

        [HttpPost("candauthenticate")]
        public async Task<IActionResult> CandidateAuthenticate(AuthenticateRequest model)
        {
            try
            {
                var response = _userService.CandidateAuthenticate(model);
            if (response == null)
                return BadRequest(new { Message = "Invalid Credentials" });

            var identity = new ClaimsIdentity("Form");
            identity.AddClaim(new Claim(ClaimTypes.Sid, Convert.ToString(response.Token)));
            identity.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(response.SID)));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Candidate"));
            var principal = new ClaimsPrincipal(identity);
            Request.HttpContext.Items["MicroUser"] = new AuthTokenResponse
            {
                Token = response.Token,
                SID = response.SID,
                Role = "Candidate"
            };

            await Request.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            return Ok(new { authToken = response.Token });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }
        }

        [HttpPost("mobileauthenticate")]
        public async Task<IActionResult> MobileAuthenticate(AuthenticateMobileRequest model)
        {
            try
            {
                //string dcryptpass = _userService.DecryptStringAES(System.Net.WebUtility.UrlDecode(authRequest.password));
                //string hashedData = _userService.ComputeStringToSha256Hash(dcryptpass);
                //var remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
               
                AuthenticateResponse authenticateResponse = _userService.MobileAuthenticate(model);
                if (authenticateResponse == null)
                    return BadRequest(new { Message = "Invalid Credentials" });

                var identity = new ClaimsIdentity("Form");
                identity.AddClaim(new Claim(ClaimTypes.Sid, Convert.ToString(authenticateResponse.Token)));
                identity.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(authenticateResponse.SID)));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Employee"));
                var principal = new ClaimsPrincipal(identity);
                Request.HttpContext.Items["MicroUser"] = new AuthTokenResponse
                {
                    Token = authenticateResponse.Token,
                    SID = authenticateResponse.SID,
                    Role = "Employee"
                };

                await Request.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return Ok(new { authToken = authenticateResponse.Token,
                    ischg = authenticateResponse.ChangePassReq
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }
             
        }

        [HttpPost("mobileusercheck")]
        public async Task<IActionResult> MobileUserCheck(AuthMobileUserCheck model)
        {
            try
            {
                Microsoft.Extensions.Primitives.StringValues authTokens; 
                HttpContext.Request.Headers.TryGetValue("mobile-auth", out authTokens);
                AuthenticateResponse authenticateResponse = _userService.MobileUserCheck(authTokens, model);
                if (authenticateResponse == null)
                    return BadRequest(new { Message = "Invalid Credentials" });

                var identity = new ClaimsIdentity("Form");
                identity.AddClaim(new Claim(ClaimTypes.Sid, Convert.ToString(authenticateResponse.Token)));
                identity.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(authenticateResponse.SID)));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Employee"));
                var principal = new ClaimsPrincipal(identity);

                Request.HttpContext.Items["MicroUser"] = new AuthTokenResponse
                {
                    Token = authenticateResponse.Token,
                    SID = authenticateResponse.SID,
                    Role = "Employee"
                };

                await Request.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return Ok(new
                {
                    authToken = authenticateResponse.Token,
                    ischg = authenticateResponse.ChangePassReq
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { message = ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
            }

        } 

    }
    
}
