﻿using MicroServices.Helpers;
using MicroServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;


namespace MicroServices.Controllers
{
    [Route("/api/micros")]
    [ApiController]
    public class MicrosController : ControllerBase
    {
        private readonly AppSettings _appSettings;

        public MicrosController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [Route("{name}/GetIPAddress")]
        public async Task<IActionResult> CommonOptions(string name)
        {
            try
            {
                var ip = HttpContext.Connection.RemoteIpAddress.ToString();
                var IPAddress = await Task.FromResult(HttpUtility.UrlEncode(StringEncryptChiper.Encrypt(ip, _appSettings.EncryptPassword)));
                return Ok(IPAddress);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
