﻿using MicroServices.Helpers;
using MicroServices.Models;
using MicroServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroServices.Controllers
{
    [Route("api/userdata")]
    [ApiController]
    public class WFMController : ControllerBase
    {
        private IUserService _userService;
        private readonly AppSettings _appSettings;
        public WFMController(IUserService userService, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [HttpPost("userinfo")]
        public async Task<IActionResult> UserInformation(AuthenticateKeyRequired model)
        {
            try
            {
                var IPAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                Microsoft.Extensions.Primitives.StringValues api_chill_key;
                HttpContext.Request.Headers.TryGetValue("X-Api-Key", out api_chill_key);
                var IPAppressList = _appSettings.AllowIP.Split(",").ToList();

                var IPItem = IPAppressList.Where(x => x == IPAddress).FirstOrDefault();

                if (_appSettings.MailMergeKey != api_chill_key)
                {
                    return BadRequest(new { Message = "Invalid Access" });
                }
                if (System.String.IsNullOrEmpty(IPItem))
                {
                    return BadRequest(new { Message = "Invalid IP Access" });
                } 

                return Ok(await Task.FromResult(_userService.UserInformation<object>("")));

            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() }); 
            }

        } 

    }
}
