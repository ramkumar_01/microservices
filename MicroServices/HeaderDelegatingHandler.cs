﻿using MicroServices.Models;
using Microsoft.AspNetCore.Http; 
using System.Net.Http; 
using System.Threading;
using System.Threading.Tasks;

public class HeaderDelegatingHandler : DelegatingHandler
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public HeaderDelegatingHandler(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    { 
        var MicroUser =(AuthTokenResponse)_httpContextAccessor.HttpContext.Items["MicroUser"];
        var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        var host = _httpContextAccessor.HttpContext.Request.Host.ToString();
        var referer = _httpContextAccessor.HttpContext.Request.GetTypedHeaders().Referer != null ? 
                _httpContextAccessor.HttpContext.Request.GetTypedHeaders().Referer.Authority != null ? 
                _httpContextAccessor.HttpContext.Request.GetTypedHeaders().Referer.Authority.ToString() : "" : "";

        request.Headers.Remove("Authorization");
        if (MicroUser != null)
        { 
            request.Headers.Add("Authorization", MicroUser.Token);
            if (MicroUser.Role == "Candidate") 
                request.Headers.Add("primitive-auth-token", MicroUser.Token);
        }
        request.Headers.Add("MyIpAddress", ip);
        request.Headers.Add("Hostname", host);
        if(referer != "")request.Headers.Add("URLReferer", referer + "_ChillSoft@2018");
        else request.Headers.Add("URLReferer", ""); 

        return await base.SendAsync(request, cancellationToken);
    }
}
