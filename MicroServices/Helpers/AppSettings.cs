﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroServices.Helpers
{
    public class AppSettings
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string TokenExpiry { get; set; }
        public string ApplicationFilePrefix { get; set; }
        public string SiteAddress { get; set; }
        public string MailFromName { get; set; }
        public string AdminMailID { get; set; }
        public int ClientCode { get; set; }
        public string ImageAPI { get; set; }
        public string EncryptPassword { get; set; }
        public string MailMergeKey { get; set; } 
        public string AllowIP { get; set; }
    }
}
