﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroServices.Helpers
{
    public class ChillSession
    {
        public int UserCode { get; set; }
        public string TypeOfUser { get; set; }
        public string TypeOfRole { get; set; }
        public Guid SID { get; set; }
        public bool LoggedStatus { get; set; }
        public string IPAddress { get; set; }
        public string EmpID { get; set; }
        public string DeviceID { get; set; }
    }

    public class AuthRequest
    {
        public string username { get; set; }
        public string password { get; set; } 

    }

    public class MobileAuthRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string DeviceID { get; set; }
        public string Androidversion { get; set; }
        public string Model { get; set; }
        public string Manufacture { get; set; }
        public string DeviceName { get; set; }
        public string BuildNo { get; set; }
        public string AutoUpdate { get; set; }

    }
}
