﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System; 
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MicroServices.Services; 
using MicroServices.Models;

public class CustomCookieAuthenticationEvents : CookieAuthenticationEvents
{
    private const string TicketIssuedTicks = nameof(TicketIssuedTicks);

    private readonly IUserService _userRepository;

    public CustomCookieAuthenticationEvents(IUserService userRepository)
    {
        _userRepository = userRepository;
    }

    public override async Task SigningIn(CookieSigningInContext context)
    {
        //IST Time Add 5:30 Minutes
        
        context.Properties.SetString(
            TicketIssuedTicks,
            DateTimeOffset.UtcNow.AddHours(5).AddMinutes(30).Ticks.ToString());

        await base.SigningIn(context);
    }

    public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
    {
        if (context == null) throw new System.ArgumentNullException(nameof(context));
        string SID, Token, Role;
        SID = context.Principal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Name)?.Value;
        Token = context.Principal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Sid)?.Value;
        Role = context.Principal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Role)?.Value;

        //var ticketIssuedTicksValue = context
        //    .Properties.GetString(TicketIssuedTicks);

        //if (ticketIssuedTicksValue is null ||
        //    !long.TryParse(ticketIssuedTicksValue, out var ticketIssuedTicks))
        //{
        //    context.RejectPrincipal();
        //    await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //    return;
        //}

        //var ticketIssuedUtc =
        //    new DateTimeOffset(ticketIssuedTicks, TimeSpan.FromHours(0));

        //if (DateTimeOffset.UtcNow.AddHours(5).AddMinutes(30) - ticketIssuedUtc > TimeSpan.FromDays(1))
        //{
        //    context.RejectPrincipal();
        //    await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //    return;
        //} 

        if (string.IsNullOrEmpty(SID))
        {
            context.RejectPrincipal();
            await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return;
        } 
        try
        {
            var user = _userRepository.GetById(SID);
            if (user == null)
            {
                context.RejectPrincipal();
                await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return;
            }
            else
            {
                if (!user.Logged_status)
                {
                    context.RejectPrincipal();
                    await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    return;
                }
                context.HttpContext.Items["MicroUser"] = new AuthTokenResponse
                {
                    Token = Token,
                    SID = SID,
                    Role = Role
                }; ;
                return;
            }
        }
        catch (Exception ex)
        {
            context.RejectPrincipal();
            await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return;
        }
    }
} 