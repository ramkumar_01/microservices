﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MicroServices.Models
{
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class AuthenticateOTPRequired
    {
        [Required]
        public string OTP { get; set; }

    } 

    public class SSOAuthenticateRequest
    {
        [Required]
        public string Email { get; set; }
         
    }


    public class AuthenticateForgetRequest
    {
        [Required]
        public string Username { get; set; } 
    }


    public class AuthenticateKeyRequired
    {
        [Required]
        public string authKey { get; set; }

    }


    public class AuthenticateChangePWDRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string OTP { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }


    public class AuthenticateMobileRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
         
        public string ClientCode { get; set; }
        
        public string Regid { get; set; }
        
        public string InstallationID { get; set; }
        
        public string DeviceType { get; set; }
       
        public string DeviceName { get; set; }
        
        public string Version { get; set; }
        
        public string OSModel { get; set; }
        
        public string OSVersion { get; set; }
        
        public string Processor { get; set; }
        
        public string RAM { get; set; }
        
        public string IMEINO1 { get; set; }
        
        public string IMEINO2 { get; set; }
        
        public string MACAddress { get; set; }
        
        public string ChillVersion { get; set; }
       
        public string fcmToken { get; set; }
        
        public string Device_SID { get; set; } 
    }

    public class AuthMobileUserCheck
    {          
        public string InstallationID { get; set; } 

        public string fcmToken { get; set; } 

    }


    public class CS_SessionClass
    {
        [Key]
        public Guid SID { get; set; }
        public int Usercode { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime modifiedDate { get; set; }

    }

    public class CS_SessionLogOutClass
    {
        public bool SSOLogin { get; set; }
        public string TypeOfUser { get; set; }
        public string Empid { get; set; } 
        public bool IsLogOutCheck { get; set; }
    }

    public class CS_CheckSessionCls
    {
        //USERCODE
        //EMPID
        //Name
        //EMAIL_ID
        //TYPEOFUSER
        //Session_TypeofRole
        //Session_Role_Code
        //Logged_status
        //AutoLogout
        //Session_UserType
        //HelpdeskRole
        //SuperAdmin
        //MobileLogin
        [Key]
        public int USERCODE { get; set; }
        public string EMPID { get; set; }
        public string Name { get; set; }
        public string EMAIL_ID { get; set; }
        public string TYPEOFUSER { get; set; }
        public string Session_TypeofRole { get; set; }
        public int? Session_Role_Code { get; set; }
        public bool Logged_status { get; set; }
        public bool AutoLogout { get; set; }
        public string Session_UserType { get; set; }
        public bool HelpdeskRole { get; set; }
        [JsonIgnore]
        public bool SuperAdmin { get; set; }
        public bool MobileLogin { get; set; }

        [JsonIgnore]
        public Guid SID { get; set; }

        // [JsonIgnore]
        public string IPAddress { get; set; }

    }
}
