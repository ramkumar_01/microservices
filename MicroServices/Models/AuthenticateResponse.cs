﻿
namespace MicroServices.Models
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public int ChangePassReq { get; set; }
        public string Token { get; set; }
        public string SID { get; set; }
        public AuthenticateResponse(dynamic user, AuthTokenResponse token)
        {
            Id = user.UserCode;
            FirstName = user.First_Name;
            LastName = user.Last_Name;
            Username = user.Name;
            ChangePassReq = user.ChangePassReq;
            Token = token.Token;
            SID = token.SID; 
        }

        public AuthenticateResponse(dynamic user, bool forget)
        { 
            FirstName = user.FirstName;
            LastName = user.LastName; 

        }

    }

    public class AuthenticateOTPResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public int ChangePassReq { get; set; }
        public string Token { get; set; }
        public string SID { get; set; }
        public AuthenticateOTPResponse(dynamic user)
        {
            Id = user.UserCode;
            FirstName = user.First_Name;
            LastName = user.Last_Name;
            Username = user.Name;
            ChangePassReq = user.ChangePassReq; 
        }
         

    }

    public class AuthenticateKeyValidation
    {

        public bool errorFlag { get; set; }
        public string ErrMessage { get; set; }
    }

    public class AuthTokenResponse
    {
        public string SID { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
    }

    public class Saml20Request
    {
        public string SamlRequest { get; set; }
        public string RelayState { get; set; }
    }
}
