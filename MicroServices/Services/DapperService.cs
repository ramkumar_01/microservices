﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.IO; 
using Microsoft.AspNetCore.Http;  
using Microsoft.Extensions.Options;
using MicroServices.Helpers;
using System.Web;

namespace MicroServices.Services
{
    public interface IDapper : IDisposable
    {
        DbConnection GetDbconnection();
        T Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);
        List<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);
        DataTable ExecuteReader(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);
        T Insert<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);
        T Update<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);
        List<T> ListUpdate<T>(string sp, List<DynamicParameters> Listparms, CommandType commandType = CommandType.StoredProcedure);
        string ReadBody();
        DbType GetDbType(string typestr);
        DbType GetAPIDbType(string typestr);  
        void SecurityParameters(DynamicParameters para, bool SIDParam = false, bool IPAddrParam = false, bool CandLogin = false);

        void MailSend(string SPName, int SPNameCode, string KeyParameters, object result, HttpContext httpcontext, bool security = true);

    }

    public class DapperServices : IDapper
    {
        private readonly IConfiguration _config;
        private string Connectionstring = "";
        private string ApplicationDS = "ConnectionString:ApplicationDS";
        private string DBAccessNumber = "ConnectionString:DBAccessNumber";
        private string DBAccessText = "ConnectionString:DBAccessText";
        private string DBAccessChar = "ConnectionString:DBAccessChar";
        private readonly AppSettings _appSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IEmailService _emailService;
        public DapperServices(IConfiguration config, IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor,
            IEmailService emailService)
        {
            _config = config;
            _appSettings = appSettings.Value;
            _httpContextAccessor = httpContextAccessor;
            _emailService = emailService;
            //Connectionstring = Convert.ToString(_config[ApplicationDS]);
            Connectionstring = Convert.ToString(_config[ApplicationDS]).Replace("[CHILLSOFT_DYNAMIC_PASSWORD]", _config[DBAccessText] + _config[DBAccessChar] + _config[DBAccessNumber]);
        }
        public void Dispose()
        {

        }
        public DataTable ExecuteReader(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            using IDbConnection db = new SqlConnection(Connectionstring);
            DataTable dt = new DataTable();
            dt.Load(db.ExecuteReader(sp, parms, commandType: commandType));
            return dt;
        }
        public T Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = new SqlConnection(Connectionstring);
            try
            {
                return db.Query<T>(sp, parms, commandType: commandType).FirstOrDefault();

            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        public List<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            using IDbConnection db = new SqlConnection(Connectionstring);
            return db.Query<T>(sp, parms, commandType: commandType).ToList();
        }
        public DbConnection GetDbconnection()
        {
            return new SqlConnection(Connectionstring);
        }
        public T Insert<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            T result;
            using IDbConnection db = new SqlConnection(Connectionstring);
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();
                //using var tran = db.BeginTransaction();
                try
                {
                    result = db.Query<T>(sp, parms, commandType: commandType).FirstOrDefault();
                    //tran.Commit();
                }
                catch (Exception ex)
                {
                    //tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;
        }

        public T Update<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            T result = default(T);
            using IDbConnection db = new SqlConnection(Connectionstring);
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    result = db.Query<T>(sp, parms, commandType: commandType, transaction: tran).FirstOrDefault();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }
            return result;
        }

        public List<T> ListUpdate<T>(string sp, List<DynamicParameters> Listparms, CommandType commandType = CommandType.StoredProcedure)
        {
            List<T> result = new List<T>();
            using IDbConnection db = new SqlConnection(Connectionstring);
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    foreach (object parms in Listparms)
                    {
                        T myresult = default(T);
                        myresult = db.Query<T>(sp, parms, commandType: commandType, transaction: tran).FirstOrDefault();
                        result.Add(myresult);
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }
            return result;
        }

        public void MailSend(string SPName, int SPNameCode, string KeyParameters, object result, HttpContext httpcontext, bool security = true)
        {
            var dbPara = new DynamicParameters();
            try
            {
                if (KeyParameters != null)
                {
                    var MailParam = KeyParameters.Split(",");
                    foreach (var MailObj in MailParam)
                    {
                        if (MailObj != "")
                        {
                            var MyObj = GetProperty(result, MailObj);
                            string propname = MailObj.ToString();
                            var value = MyObj;
                            dbPara.Add("@" + propname, value, GetDbType(MyObj.GetType().Name.ToString()));
                        }
                    }
                }
                dbPara.Add("@SPNameCode", SPNameCode, DbType.Int32);
                if (security) SecurityParameters(dbPara, false);

                var ListMailResult = GetAll<object>(SPName,
                           dbPara, commandType: CommandType.StoredProcedure);

                if (ListMailResult != null)
                {
                    foreach (var MailResult in ListMailResult)
                    {
                        if (MailResult != null)
                        {
                            int TemplateCode = Convert.ToInt32(GetProperty(MailResult, "TemplateCode"));
                            string ToMail = Convert.ToString(GetProperty(MailResult, "ToMail"));
                            string ToMailName = Convert.ToString(GetProperty(MailResult, "ToMailName"));
                            string Subject = Convert.ToString(GetProperty(MailResult, "Subject"));
                            string Body = Convert.ToString(GetProperty(MailResult, "Body"));
                            string BCCMail = Convert.ToString(GetProperty(MailResult, "BCCMail"));
                            string BCCName = Convert.ToString(GetProperty(MailResult, "BCCName"));
                            string CCMail = Convert.ToString(GetProperty(MailResult, "CCMail"));
                            string CCName = Convert.ToString(GetProperty(MailResult, "CCName"));
                            string FileNameAndPath = Convert.ToString(GetProperty(MailResult, "FileNameAndPath"));
                            string MailFromName = Convert.ToString(GetProperty(MailResult, "MailFromName"));
                            string AdminMailID = Convert.ToString(GetProperty(MailResult, "AdminMailID"));
                            string MailSavePath = Convert.ToString(GetProperty(MailResult, "MailSavePath"));
                            string SiteAddress = Convert.ToString(GetProperty(MailResult, "SiteAddress"));
                            string ImagePath = Convert.ToString(GetProperty(MailResult, "ImagePath"));
                            string FileName = GetProperty(MailResult, "FileName") != null ? Convert.ToString(GetProperty(MailResult, "FileName")) : "";

                            int CSMailSendCode = Convert.ToInt32(GetProperty(MailResult, "CSMailSendCode"));

                            dbPara = new DynamicParameters();
                            dbPara.Add("@CSMailSendCode", CSMailSendCode, DbType.Int32);

                            if (ToMail != "")
                            {
                                string tempBody = PlaceHolderReplace(TemplateCode, Body, result);

                                ChillEmail email = _emailService.ChillMailSend(TemplateCode, ToMail, ToMailName, Subject, tempBody, BCCMail,
                                BCCName, CCMail, CCName, FileNameAndPath,
                                MailFromName, AdminMailID, MailSavePath, SiteAddress, ImagePath, FileName);

                                dbPara.Add("@Filename", email.filename, DbType.String);
                                dbPara.Add("@Message", email.Message, DbType.String);
                                dbPara.Add("@Status", email.status, DbType.Boolean);
                            }
                            else
                            {
                                dbPara.Add("@Filename", "", DbType.String);
                                dbPara.Add("@Message", "To MailID not exists", DbType.String);
                                dbPara.Add("@Status", false, DbType.Boolean);
                            }

                            Update<object>("CS_MailSend_Update",
                                        dbPara, commandType: CommandType.StoredProcedure);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string PlaceHolderReplace(int _templatecode, string _body, object _result)
        {
            var dbPara = new DynamicParameters();
            string newbody = _body;
            string tempresult = "";
            try
            {
                dbPara.Add("@TemplateCode", _templatecode, DbType.Int32);

                var ListMailResult = GetAll<object>("CS_Common_Mail_Replace_Holder",
                           dbPara, commandType: CommandType.StoredProcedure);

                if (ListMailResult != null)
                {
                    foreach (var MailResult in ListMailResult)
                    {
                        if (MailResult != null)
                        {
                            string _dataPlaceholder = Convert.ToString(GetProperty(MailResult, "dataPlaceholder"));
                            string _fieldName = Convert.ToString(GetProperty(MailResult, "fieldName"));
                            bool _isEncryption = Convert.ToBoolean(GetProperty(MailResult, "isEncryption"));
                            bool _isDefault = Convert.ToBoolean(GetProperty(MailResult, "IsDefault"));
                            string _isDefaultValue = Convert.ToString(GetProperty(MailResult, "IsDefaultValue"));
                            if (_isDefault)
                            {
                                newbody = newbody.Replace(_dataPlaceholder, _isDefaultValue);
                            }
                            else
                            {
                                tempresult = Convert.ToString(GetProperty(_result, _fieldName));
                                if (tempresult != null && _isEncryption) tempresult = HttpUtility.UrlEncode(StringEncryptChiper.Encrypt(tempresult, _appSettings.EncryptPassword).ToString());
                                newbody = newbody.Replace(_dataPlaceholder, tempresult);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbPara = null;
            }

            return newbody;

        }
        public string ReadBody()
        {
            string body = "";
            using (StreamReader stream = new StreamReader(_httpContextAccessor.HttpContext.Request.Body))
            {
                body = stream.ReadToEnd();
            }
            return body;
        }

        public DbType GetDbType(string typestr)
        {
            string typevalue = typestr.ToUpper();

            if (typevalue == "STRING")
                return DbType.String;
            else if (typevalue == "FLOAT")
                return DbType.Double;
            else if (typevalue == "INTEGER")
                return DbType.Int32;
            else if (typevalue == "BOOLEAN")
                return DbType.Boolean;
            else if (typevalue == "DATE")
                return DbType.DateTime;
            else
                return DbType.String;
        }

        public DbType GetAPIDbType(string typestr)
        {
            string typevalue = typestr.ToUpper();

            if (typevalue == "STRING")
                return DbType.String;
            else if (typevalue == "DOUBLE")
                return DbType.Double;
            else if (typevalue == "INT32")
                return DbType.Int32;
            else if (typevalue == "BOOLEAN")
                return DbType.Boolean;
            else if (typevalue == "DATETIME")
                return DbType.DateTime;
            else
                return DbType.String;
        }
         
        public void SecurityParameters(DynamicParameters dbPara, bool SIDParam = false, bool IPAddrParam = false, bool CandLogin = false)
        {
            var user = (ChillSession)_httpContextAccessor.HttpContext.Items["MicroUser"];
            try
            {
                dbPara.Add("@MithraUserCode", user.UserCode, DbType.Int32);
                dbPara.Add("@MithraTypeOfUser", user.TypeOfUser, DbType.String);
                if (string.IsNullOrEmpty(user.TypeOfRole) == false)
                    dbPara.Add("@MithraTypeOfRole", user.TypeOfRole, DbType.String);
                else
                    dbPara.Add("@MithraTypeOfRole", DBNull.Value, DbType.String); 
                dbPara.Add("@UserDatetime", System.DateTime.Now, DbType.DateTime);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public object GetProperty(object target, string name)
        {
            var site = System.Runtime.CompilerServices.CallSite<Func<System.Runtime.CompilerServices.CallSite, object, object>>
                .Create(Microsoft.CSharp.RuntimeBinder.Binder
                .GetMember(0, name, target.GetType(), new[] { Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo.Create(0, null) }));
            return site.Target(site, target);
        }
         

    }
}
