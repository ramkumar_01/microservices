﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data;
using System.Net.Mail;
using System.Text.RegularExpressions; 
using Microsoft.Extensions.Options;
using System.Web;
using MicroServices.Helpers;

namespace MicroServices.Services
{
   

    public interface IEmailService
    {
        ChillEmail ChillMailSend(int TemplateCode, string ToMail, string ToMailName,
            string Subject, string Body, string BCCMail, string BCCName, string CCMail,
            string CCName, string FileNameAndPath, string MailFromName, string AdminMailID, 
            string MailSavePath, string SiteAddress, string ImagePath, string FileName = "");         

    }
    public class EmailService : IEmailService
    {
        private readonly AppSettings _appSettings;
        public EmailService(IOptions<AppSettings> appSettings)
        { 
            _appSettings = appSettings.Value;
        }

        public ChillEmail ChillMailSend(int TemplateCode, string ToMail, string ToMailName, 
            string Subject, string Body, string BCCMail, string BCCName, string CCMail,
            string CCName, string FileNameAndPath, string MailFromName, string AdminMailID, string MailSavePath, 
            string SiteAddress, string ImagePath, string FileName = "")
        { 
            MailMessage EMail = new MailMessage(); 
            string FolderName;
            string FromMail;
            string FromName;
            FileStream filestream = null;
            try
            {
                ToMail = Strings.Replace(ToMail.Trim(), ",", ";");
                if (Strings.Right(ToMail, 1) == ";")
                    ToMail = Strings.Left(ToMail, ToMail.Length - 1);

                if (string.IsNullOrEmpty(BCCMail) == false)
                {
                    BCCMail = Strings.Replace(BCCMail.Trim(), ",", ";");
                   // if (Strings.Right(BCCMail, 1) == ";")
                   //     BCCMail = Strings.Left(BCCMail, BCCMail.Length - 1);
                }

                if (string.IsNullOrEmpty(CCMail) == false)
                {
                    CCMail = Strings.Replace(CCMail.Trim(), ",", ";");
                    if (Strings.Right(CCMail, 1) == ";")
                        CCMail = Strings.Left(CCMail, CCMail.Length - 1);
                }
                 
                    FromName = MailFromName;
                    FromMail = AdminMailID; 
                    FolderName = "emails"; 
                 
                    EMail.Subject = Subject;
                    EMail.Body = Body;
                    EMail.BodyEncoding = System.Text.Encoding.UTF8;
                    EMail.IsBodyHtml = true;
                    EMail.From = new MailAddress(FromMail, FromName);
                    // To add TO email
                    if (Strings.InStr(ToMail, ";") > 0)
                    {
                        foreach (string address in Strings.Split(ToMail, ";"))
                            EMail.To.Add(address);
                    }
                    else
                        EMail.To.Add(new MailAddress(ToMail, ToMailName));

                    // To add CC email

                    if (Strings.InStr(CCMail, ";") > 0)
                    {
                        foreach (string address in Strings.Split(CCMail, ";"))
                            EMail.CC.Add(address);
                    }
                    else
                        if(CCMail != "") EMail.CC.Add(new MailAddress(CCMail, CCName));

                    // To add BCC email
                    if (string.IsNullOrEmpty(BCCMail) == false)
                    {
                        EMail.Headers.Add("rcc", BCCMail);
                        // if (Strings.Right(BCCMail, 1) == ";")
                        //     BCCMail = Strings.Left(BCCMail, BCCMail.Length - 1);
                    }
                

                   if (string.IsNullOrEmpty(FileNameAndPath) == false)
                    {
                        if (System.IO.File.Exists(FileNameAndPath) == true)
                        {
                            filestream = new FileStream(@"" + FileNameAndPath, FileMode.Open, FileAccess.Read);
                            EMail.Attachments.Add(new Attachment(filestream, FileName)); 
                        } 
                    }

                    List<string> listOfImgdata;
                    string htmlSource;
                    string Tempitem;
                    listOfImgdata = CSFetchImgsFromSource(Body);
                    htmlSource = Body;
                    foreach (string item in listOfImgdata)
                    {
                        Tempitem = Strings.Replace(item, "..", "");
                        Tempitem = Strings.Replace(Tempitem, SiteAddress, "");
                        if (System.IO.File.Exists(ImagePath + @"\" + Tempitem))  
                        {
                            if (Strings.InStr(htmlSource, item) > 0)
                            {
                                Attachment inline = new Attachment(ImagePath + @"\" + Tempitem, "image/jpeg");
                                htmlSource = Regex.Replace(htmlSource, item, "cid:" + inline.ContentId);
                                EMail.Attachments.Add(inline);
                            }
                        }
                        Body = htmlSource;
                    }
                    string ImageCode = "CHILL_" + TemplateCode.ToString() + "_" + _appSettings.ClientCode.ToString();
                    Body = Body + @"<img style='display:none' src='" + _appSettings.ImageAPI + @"k="
                    + HttpUtility.UrlEncode(StringEncryptChiper.Encrypt(ImageCode, _appSettings.EncryptPassword))
                    + @"' />";
                    EMail.Body = Body;

                String guid = System.Guid.NewGuid().ToString();
                EMail.Save(MailSavePath + @"\" + guid + ".eml");

                return new ChillEmail(){filename = guid, Message = "Mail Sent!", status = true};

            }
            catch (Exception ex)
            {
                return new ChillEmail() { filename = "", Message = ex.Message, status = false };
            }
            finally
            { 
                EMail.Dispose(); 
                EMail = null;
                if(filestream != null)
                {
                    filestream.Dispose();
                    filestream.Close();
                }
            }             
        }

        private List<string> CSFetchImgsFromSource(string htmlSource)
        {
            List<string> listOfImgdata = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                listOfImgdata.Add(href);
            }

            return listOfImgdata;
        }
    }

    public static class MailMessageExt
    {
        public static void Save(this MailMessage Message, string FileName)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type _mailWriterType =
              assembly.GetType("System.Net.Mail.MailWriter");

            using (FileStream _fileStream =
                   new FileStream(FileName, FileMode.Create))
            {
                // Get reflection info for MailWriter contructor
                ConstructorInfo _mailWriterContructor =
                    _mailWriterType.GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[] { typeof(Stream) },
                        null);

                // Construct MailWriter object with our FileStream
                object _mailWriter =
                  _mailWriterContructor.Invoke(new object[] { _fileStream });

                // Get reflection info for Send() method on MailMessage
                MethodInfo _sendMethod =
                    typeof(MailMessage).GetMethod(
                        "Send",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call method passing in MailWriter
                _sendMethod.Invoke(
                    Message,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { _mailWriter, true, true },
                    null);

                // Finally get reflection info for Close() method on our MailWriter
                MethodInfo _closeMethod =
                    _mailWriter.GetType().GetMethod(
                        "Close",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call close method
                _closeMethod.Invoke(
                    _mailWriter,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { },
                    null);
            }
        }
    }

    public class ChillEmail
    {
        public string filename { get; set; }
        public bool status { get; set; }
        public string Message { get; set; }
    }
}
