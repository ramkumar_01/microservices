﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text; 
using MicroServices.Helpers;
using MicroServices.Models;
using Microsoft.AspNetCore.Http;
using Dapper;
using System.Linq;
using System.Collections.Generic;

namespace MicroServices.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model, HttpContext httpContext);

        AuthenticateOTPResponse OTPAuthenticate(AuthenticateOTPRequired model, string SID);

        AuthenticateOTPResponse OTPReTrigger(string SID, HttpContext httpContext);

        AuthenticateResponse MobileAuthenticate(AuthenticateMobileRequest model);

        AuthenticateResponse MobileUserCheck(string Token, AuthMobileUserCheck model);
        CS_CheckSessionCls GetById(string MyGuid, string ipaddr = "");

        CS_SessionLogOutClass LogOut(string MyGuid);

        AuthenticateResponse CandidateAuthenticate(AuthenticateRequest model);

        AuthenticateResponse ForgetPassword(AuthenticateForgetRequest model, HttpContext httpContext);

        AuthenticateResponse ChangePassword(AuthenticateChangePWDRequest model);

        AuthenticateResponse SSOAuthenticate(string Email);

        AuthenticateKeyValidation OutSideAccessValidation(AuthenticateKeyRequired authenticateKeyRequired, 
            string AuthPassword, string Url, string IPAddress);

        List<T> UserInformation<T>(string Active);

    }
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings; 
        private readonly IDapper _dapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string  _IPAddress;
        public UserService(IOptions<AppSettings> appSettings, IDapper dapper, IHttpContextAccessor httpContextAccessor)
        {
            _appSettings = appSettings.Value; 
            _dapper = dapper;
            _httpContextAccessor = httpContextAccessor;
            _IPAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        public AuthenticateKeyValidation OutSideAccessValidation(AuthenticateKeyRequired authenticateKeyRequired, 
            string AuthPassword, string Url, string IPAddress)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@authKey", authenticateKeyRequired.authKey, System.Data.DbType.String);
            dbPara.Add("@Password", AuthPassword, System.Data.DbType.String);
            dbPara.Add("@AuthURL", Url, System.Data.DbType.String);
            dbPara.Add("@IPAddress", IPAddress, System.Data.DbType.String);
            return _dapper.Get<AuthenticateKeyValidation>("CSOutside_API_Service_Check", dbPara, System.Data.CommandType.StoredProcedure);
            
        }

        public List<T> UserInformation<T>(string Active)
        { 
            var dbPara = new DynamicParameters();
            dbPara.Add("@Active", (Active != "") ? "" : Active, System.Data.DbType.String);
            return _dapper.GetAll<T>("CSOutside_API_Service_UserInfo", dbPara, System.Data.CommandType.StoredProcedure);

        }

        public AuthenticateResponse CandidateAuthenticate(AuthenticateRequest model)
        {
            var dbPara = new DynamicParameters(); 
            dbPara.Add("@Username", model.Username, System.Data.DbType.String);
            dbPara.Add("@Password", model.Password, System.Data.DbType.String);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);

            var user = _dapper.Get<dynamic>("CSTAL_Candidate_UserDetail", dbPara, System.Data.CommandType.StoredProcedure);

            if (user == null) return null;
            var token = generateJwtToken(user, mobile: false, prn: "cport");
            return new AuthenticateResponse(user, token);
        }
        public AuthenticateResponse Authenticate(AuthenticateRequest model, HttpContext httpContext)
        {
            var dbPara = new DynamicParameters(); 
            dbPara.Add("@Username", model.Username, System.Data.DbType.String);
            dbPara.Add("@Password", model.Password, System.Data.DbType.String);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);

            var user = _dapper.Get<dynamic>("CS_Get_UserDetail", dbPara, System.Data.CommandType.StoredProcedure);

            if (user == null) return null;
            if (user.ChangePassReq == 100)
            {
                _dapper.MailSend("CSWFM_OTPSend_Invite_Mail", 10000, "UserCode", user, httpContext, false);

            }
            var token = generateJwtToken(user);
            return new AuthenticateResponse(user, token);
        }

        public AuthenticateOTPResponse OTPAuthenticate(AuthenticateOTPRequired model, string SID)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@OTP", model.OTP, System.Data.DbType.String);
            dbPara.Add("@MyId", new Guid(SID), System.Data.DbType.Guid);
            var user = _dapper.Get<dynamic>("CS_Get_OTPUserDetail", dbPara, System.Data.CommandType.StoredProcedure);

            if (user == null) return null;
            else return new AuthenticateOTPResponse(user);
        }

        public AuthenticateOTPResponse OTPReTrigger(string SID, HttpContext httpContext)
        {
            var dbPara = new DynamicParameters(); 
            dbPara.Add("@MyId", new Guid(SID), System.Data.DbType.Guid);
            var user = _dapper.Get<dynamic>("CS_Get_OTPRetrigger", dbPara, System.Data.CommandType.StoredProcedure);

            _dapper.MailSend("CSWFM_OTPSend_Invite_Mail", 10000, "UserCode", user, httpContext, false);

            if (user == null) return null;
            else return new AuthenticateOTPResponse(user);
        }

        public AuthenticateResponse SSOAuthenticate(string Email)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@Username", Email, System.Data.DbType.String);
            dbPara.Add("@Password", "", System.Data.DbType.String);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);
            dbPara.Add("@SSOLogin", true, System.Data.DbType.Boolean);

            var user = _dapper.Get<dynamic>("CS_Get_UserDetail", dbPara, System.Data.CommandType.StoredProcedure);

            if (user == null) return null;
            var token = generateJwtToken(user, ssoLogin:true);
            return new AuthenticateResponse(user, token);
        }

        public AuthenticateResponse ForgetPassword(AuthenticateForgetRequest model, HttpContext httpContext)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@Username", model.Username, System.Data.DbType.String);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);

            var user = _dapper.Get<dynamic>("CSWFM_Forgetpassword", dbPara, System.Data.CommandType.StoredProcedure);
            if (user == null) return null;

            _dapper.MailSend("CSWFM_Forgetpassword_Invite_Mail", 10000, "PasswordCode", user, httpContext, false);
            return new AuthenticateResponse(user, true);
        }

        public AuthenticateResponse ChangePassword(AuthenticateChangePWDRequest model)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@Username", model.Username, System.Data.DbType.String);
            dbPara.Add("@OTP", model.OTP, System.Data.DbType.String);
            dbPara.Add("@NewPassword", model.NewPassword, System.Data.DbType.String);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);

            var user = _dapper.Get<dynamic>("CSWFM_Password_User_Update", dbPara, System.Data.CommandType.StoredProcedure);
            if (user == null) return null;

            return new AuthenticateResponse(user, true);
        }

        public AuthenticateResponse MobileAuthenticate(AuthenticateMobileRequest model)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@Username", model.Username, System.Data.DbType.String);
            dbPara.Add("@Password", model.Password, System.Data.DbType.String);
            dbPara.Add("@ClientCode", model.ClientCode, System.Data.DbType.String);
            dbPara.Add("@Regid", model.Regid, System.Data.DbType.String);
            dbPara.Add("@InstallationID", model.InstallationID, System.Data.DbType.String);
            dbPara.Add("@DeviceType", model.DeviceType, System.Data.DbType.String);
            dbPara.Add("@DeviceName", model.DeviceName, System.Data.DbType.String);
            dbPara.Add("@Version", model.Version, System.Data.DbType.String);
            dbPara.Add("@OSModel", model.OSModel, System.Data.DbType.String);
            dbPara.Add("@OSVersion", model.OSVersion, System.Data.DbType.String);
            dbPara.Add("@Processor", model.Processor, System.Data.DbType.String);
            dbPara.Add("@RAM", model.RAM, System.Data.DbType.String);
            dbPara.Add("@IMEINO1", model.IMEINO1, System.Data.DbType.String);
            dbPara.Add("@IMEINO2", model.IMEINO2, System.Data.DbType.String);
            dbPara.Add("@MACAddress", model.MACAddress, System.Data.DbType.String);
            dbPara.Add("@ChillVersion", model.ChillVersion, System.Data.DbType.String);
            dbPara.Add("@fcmToken", model.fcmToken, System.Data.DbType.String);
            dbPara.Add("@Device_SID", model.Device_SID, System.Data.DbType.String);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);

            var user = _dapper.Get<dynamic>("CS_Get_MobileUserDetail", dbPara, System.Data.CommandType.StoredProcedure);

            if (user == null) return null;
            var token = mobilegenerateJwtToken(user, user.DeviceUserCode);
            return new AuthenticateResponse(user, token);
        }

        public AuthenticateResponse MobileUserCheck(string Token, AuthMobileUserCheck model)
        {
            var dbPara = new DynamicParameters();
           // string MyGuid = attachUserToContext(Token); 
            dbPara.Add("@InstallationID", model.InstallationID, System.Data.DbType.String);
            dbPara.Add("@fcmToken", model.fcmToken, System.Data.DbType.String);

            var user = _dapper.Get<dynamic>("CS_Get_MobileUserInfo", dbPara, System.Data.CommandType.StoredProcedure);

            if (user == null) return null;
            var token = mobilegenerateJwtToken(user, user.DeviceUserCode);
            return new AuthenticateResponse(user, token);
        }
        public CS_CheckSessionCls GetById(string MyGuid, string ipaddr = "")
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@MyId", new Guid(MyGuid), System.Data.DbType.Guid);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);

            var lstchecksession = _dapper.Get<CS_CheckSessionCls>("CheckSession", dbPara, System.Data.CommandType.StoredProcedure);
            if (lstchecksession.Logged_status)
            {
                lstchecksession.IPAddress = ipaddr;
                _dapper.Get<dynamic>("UpdateSession", dbPara, System.Data.CommandType.StoredProcedure);

                return lstchecksession;
            }
            else
            {
                return null;
            }
        }

        public CS_SessionLogOutClass LogOut(string MyGuid)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("@MyId", new Guid(MyGuid), System.Data.DbType.Guid);

            return _dapper.Get<CS_SessionLogOutClass>("CSSessionLogout", dbPara,
                System.Data.CommandType.StoredProcedure); 
        }


        private AuthTokenResponse mobilegenerateJwtToken(object param, int DeviceUserCode = 0)
        {
            dynamic user = param;
            var MyGuid = Guid.NewGuid().ToString();
            string usercode = user.UserCode.ToString();
            var claims = new Claim[] {
                new Claim(JwtRegisteredClaimNames.Sub, usercode),
                new Claim(JwtRegisteredClaimNames.Sid, MyGuid),
                new Claim(JwtRegisteredClaimNames.Prn, "uport")
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var issuer = _appSettings.Issuer;
            var audience = _appSettings.Audience;
            var jwtValidity = DateTime.Now.AddDays(Convert.ToDouble(_appSettings.TokenExpiry));

            var token = new JwtSecurityToken(issuer,
              audience,
              claims,
              expires: jwtValidity,
              signingCredentials: creds);

            var dbPara = new DynamicParameters();
            dbPara.Add("@MyId", new Guid(MyGuid), System.Data.DbType.Guid);
            dbPara.Add("@UserCode", user.UserCode, System.Data.DbType.Int32);
            dbPara.Add("@TypeOfUser", user.TypeOfUser, System.Data.DbType.String);
            dbPara.Add("@MobileFlag", true, System.Data.DbType.Boolean);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);
            dbPara.Add("@ssoLogin", false, System.Data.DbType.Boolean);
            dbPara.Add("@DeviceUserCode", DeviceUserCode, System.Data.DbType.Int32);

            _dapper.Get<dynamic>("CS_Create_MobileSession", dbPara, System.Data.CommandType.StoredProcedure);

            return new AuthTokenResponse { Token = new JwtSecurityTokenHandler().WriteToken(token), SID = MyGuid };
        }


        private AuthTokenResponse generateJwtToken(object param, bool mobile = false, string prn = "uport",bool ssoLogin = false)
        {
            dynamic user = param;
            var MyGuid = Guid.NewGuid().ToString();
            string usercode = user.UserCode.ToString();
            var claims = new Claim[] {
                new Claim(JwtRegisteredClaimNames.Sub, usercode),
                new Claim(JwtRegisteredClaimNames.Sid, MyGuid),
                new Claim(JwtRegisteredClaimNames.Prn, prn)
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var issuer = _appSettings.Issuer;
            var audience = _appSettings.Audience;
            var jwtValidity = DateTime.Now.AddDays(Convert.ToDouble(_appSettings.TokenExpiry));

            var token = new JwtSecurityToken(issuer,
              audience,
              claims,
              expires: jwtValidity,
              signingCredentials: creds);

            var dbPara = new DynamicParameters();
            dbPara.Add("@MyId", new Guid(MyGuid), System.Data.DbType.Guid);
            dbPara.Add("@UserCode", user.UserCode, System.Data.DbType.Int32);
            dbPara.Add("@TypeOfUser", user.TypeOfUser, System.Data.DbType.String);
            dbPara.Add("@MobileFlag", mobile, System.Data.DbType.Boolean);
            dbPara.Add("@IpAddress", _IPAddress, System.Data.DbType.String);
            dbPara.Add("@ssoLogin", ssoLogin, System.Data.DbType.Boolean);
            if (prn == "uport")
                _dapper.Get<dynamic>("CS_Create_Session", dbPara, System.Data.CommandType.StoredProcedure);
            else
                _dapper.Get<dynamic>("CSTAL_Candidate_Create_Session", dbPara, System.Data.CommandType.StoredProcedure);
             
            return new AuthTokenResponse { Token = new JwtSecurityTokenHandler().WriteToken(token), SID = MyGuid };
        }

        private AuthTokenResponse generateJwtTokenWConnect(object param, string MyGuid)
        {
            dynamic user = param; 
            string usercode = user.UserCode.ToString();
            var claims = new Claim[] {
                new Claim(JwtRegisteredClaimNames.Sub, usercode),
                new Claim(JwtRegisteredClaimNames.Sid, MyGuid),
                new Claim(JwtRegisteredClaimNames.Prn, "uport")
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var issuer = _appSettings.Issuer;
            var audience = _appSettings.Audience;
            var jwtValidity = DateTime.Now.AddDays(Convert.ToDouble(_appSettings.TokenExpiry));

            var token = new JwtSecurityToken(issuer,
              audience,
              claims,
              expires: jwtValidity,
              signingCredentials: creds);
             
            return new AuthTokenResponse { Token = new JwtSecurityTokenHandler().WriteToken(token), SID = MyGuid };
        }

        private string attachUserToContext(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    LifetimeValidator = CustomLifetimeValidator,
                    RequireExpirationTime = true
                }, out SecurityToken validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                return jwtToken.Claims.First(x => x.Type == "sid").Value.ToString(); 
                 
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private bool CustomLifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken tokenToValidate, TokenValidationParameters @param)
        {
            //if (expires != null)
            //{
            //    return expires < DateTime.UtcNow;
            //}
            return true;
        }
    }
}
